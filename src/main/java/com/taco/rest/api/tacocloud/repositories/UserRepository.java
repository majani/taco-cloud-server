package com.taco.rest.api.tacocloud.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.taco.rest.api.tacocloud.models.User;

public interface UserRepository extends JpaRepository<User, Long> {
	User findByUsername(String user);

}