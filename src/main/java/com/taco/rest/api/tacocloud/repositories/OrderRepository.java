package com.taco.rest.api.tacocloud.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.taco.rest.api.tacocloud.models.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {

}