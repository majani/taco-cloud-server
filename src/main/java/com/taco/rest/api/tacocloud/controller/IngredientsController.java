package com.taco.rest.api.tacocloud.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.taco.rest.api.tacocloud.models.Ingredient;
import com.taco.rest.api.tacocloud.repositories.IngredientRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/ingredients")
public class IngredientsController {

	@Autowired
	IngredientRepository ingRepo;

	@GetMapping
	public List<Ingredient> lisIngredients() {
		return ingRepo.findAll();
	}

	@GetMapping("/{id}")
	public ResponseEntity<Ingredient> getIngredient(@PathVariable String id) {
		Optional<Ingredient> ingredient = ingRepo.findById(id);
		if (ingredient.isPresent()) {
			return new ResponseEntity<Ingredient>(ingredient.get(), HttpStatus.OK);
		}

		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}

	@PostMapping
	public ResponseEntity<Ingredient> createIngredient(@RequestBody Ingredient ingredient) {
		Ingredient newIng = ingRepo.save(ingredient);

		return new ResponseEntity<Ingredient>(newIng, HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Ingredient> updateIngredient(@PathVariable String id, @RequestBody Ingredient ingredient) {
		Optional<Ingredient> findIng = ingRepo.findById(id);
		if (findIng.isPresent()) {
			Ingredient updatedIng = ingRepo.save(ingredient);
			return new ResponseEntity<Ingredient>(updatedIng, HttpStatus.OK);
		}

		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteIngredient(@PathVariable String id) {
		Optional<Ingredient> findIng = ingRepo.findById(id);

		if (findIng.isPresent()) {
			ingRepo.deleteById(id);
			return ResponseEntity.noContent().build();
		}

		return new ResponseEntity(null, HttpStatus.NOT_FOUND);
	}

}
