package com.taco.rest.api.tacocloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.taco.rest.api.tacocloud.jwt.AuthenticationRequest;
import com.taco.rest.api.tacocloud.jwt.AuthenticationResponse;
import com.taco.rest.api.tacocloud.jwt.JwtUtil;

@RestController
@CrossOrigin("*")
@RequestMapping("/authenticate")
public class AuthenticateController {

	@Autowired
	private AuthenticationManager authManager;

	@Autowired
	UserDetailsService userSvc;

	@Autowired
	JwtUtil jwtUtil;

	@PostMapping
	public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest)
			throws Exception {

		try {
			authManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(),
					authenticationRequest.getPassword()));
		} catch (BadCredentialsException e) {
			throw new Exception("Incorrect Username Password");

		}

		final UserDetails userDetails = userSvc.loadUserByUsername(authenticationRequest.getUsername());
		final String jwt = jwtUtil.generateToken(userDetails);
		return new ResponseEntity(new AuthenticationResponse(jwt), HttpStatus.OK);
	}

}
