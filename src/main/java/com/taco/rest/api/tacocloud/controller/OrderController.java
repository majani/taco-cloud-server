package com.taco.rest.api.tacocloud.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.taco.rest.api.tacocloud.models.Order;
import com.taco.rest.api.tacocloud.models.User;
import com.taco.rest.api.tacocloud.repositories.OrderRepository;

import lombok.extern.slf4j.Slf4j;

@RestController
@CrossOrigin("*")
@RequestMapping("/orders")
@Slf4j
public class OrderController {

	@Autowired
	OrderRepository orderRepo;
	
	@Autowired
	RabbitTemplate rabbit;

	@GetMapping()
	public Iterable<Order> getOrders() {
		return orderRepo.findAll();
	}

	@GetMapping("/{id}")
	public ResponseEntity<Order> getOrder(@PathVariable Long id) {
		Optional<Order> findOrder = orderRepo.findById(id);

		if (findOrder.isPresent()) {
			return new ResponseEntity<Order>(findOrder.get(), HttpStatus.OK);
		}

		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}

	@PostMapping
	public ResponseEntity<Order> postOrder(@RequestBody Order order, @AuthenticationPrincipal User user) {
		order.setUser(user);
		Order newOrder = orderRepo.save(order);
		rabbit.convertAndSend("tacocloud.orders.queue", newOrder);
		log.info("ORDER SAVED AND SENT TO RABBIT");
		return new ResponseEntity<Order>(newOrder, HttpStatus.CREATED);
	}

	@PutMapping("{/id}")
	public ResponseEntity<Order> putOrder(@RequestBody Order order, @PathVariable Long id) {
		Optional<Order> findOrder = orderRepo.findById(id);
		if (findOrder.isPresent()) {
			Order updatedOrder = orderRepo.save(order);
			return new ResponseEntity<Order>(updatedOrder, HttpStatus.OK);
		}

		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

	}

	@PatchMapping("/{orderId}")
	public Order patchOrder(@PathVariable Long orderId, @RequestBody Order patch) {
		Order order = orderRepo.findById(orderId).get();

		if (patch.getName() != null) {
			order.setName(patch.getName());
		}
		if (patch.getStreet() != null) {
			order.setStreet(patch.getStreet());
		}
		if (patch.getCity() != null) {
			order.setCity(patch.getCity());
		}
		if (patch.getState() != null) {
			order.setState(patch.getState());
		}
		if (patch.getZip() != null) {
			order.setZip(patch.getZip());
		}
		if (patch.getCcNumber() != null) {
			order.setCcNumber(patch.getCcNumber());
		}
		if (patch.getCcExpiration() != null) {
			order.setCcExpiration(patch.getCcExpiration());
		}
		if (patch.getCcCVV() != null) {
			order.setCcCVV(patch.getCcCVV());
		}

		return orderRepo.save(order);
	}

	@DeleteMapping("{/id}")
	public void deleteOrder(@PathVariable Long id) {
		try {
			orderRepo.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
		}
	}

}
