package com.taco.rest.api.tacocloud.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.taco.rest.api.tacocloud.models.Taco;
import com.taco.rest.api.tacocloud.repositories.TacoRepository;

import lombok.extern.slf4j.Slf4j;

@RestController
@CrossOrigin("*")
@RequestMapping(path = "/design", produces = "application/json")
@Slf4j
public class DesignTacoController {

	@Autowired
	private TacoRepository tacoRepo;
	
	@Autowired
	RabbitTemplate rabbit;

	@GetMapping("/recent")
	public Iterable<Taco> recentTacos() {
		PageRequest page = PageRequest.of(0, 16, Sort.by("createdAt").descending());
		return tacoRepo.findAll(page).getContent();
	}

	@GetMapping
	public List<Taco> getTacos() {
		return tacoRepo.findAll();
	}

	@GetMapping("/{id}")
	public ResponseEntity<Taco> getTaco(@PathVariable Long id) {
		Optional<Taco> taco = tacoRepo.findById(id);

		if (taco.isPresent()) {
			return new ResponseEntity<Taco>(taco.get(), HttpStatus.OK);
		}

		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}

	@PostMapping
	public ResponseEntity<Taco> createTaco(@RequestBody Taco taco) {
		Taco createdTaco = tacoRepo.save(taco);
		rabbit.convertAndSend("tacocloud.tacos.queue", createdTaco);
		
		
//		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(createdTaco.getId())
//				.toUri();
		log.info("TACO CREATED AND SENT TO RABBIT");
		return new ResponseEntity<Taco>(taco, HttpStatus.CREATED);
		

	}

	@PutMapping("/{id}")
	public ResponseEntity<Taco> updateTaco(@PathVariable Long id, @RequestBody Taco taco) {
		Optional<Taco> upTaco = tacoRepo.findById(id);

		if (upTaco.isPresent()) {
			Taco updatedTaco = tacoRepo.save(taco);
			log.info("TACO UPDATED");
			return new ResponseEntity<Taco>(updatedTaco, HttpStatus.OK);
		}

		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

	}

	@PatchMapping("/{id}")
	public ResponseEntity<Taco> patchTaco(@PathVariable Long id, @RequestBody Taco patch) {
		Optional<Taco> taco = tacoRepo.findById(id);

		if (taco.isPresent()) {
			Taco toBUpdtd = taco.get();
			if (patch.getName() != null) {
				toBUpdtd.setName(patch.getName());
			}

			if (patch.getIngredients() != null) {
				toBUpdtd.setIngredients(patch.getIngredients());
			}

			tacoRepo.save(toBUpdtd);

			return new ResponseEntity<Taco>(toBUpdtd, HttpStatus.OK);
		}

		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteTaco(@PathVariable Long id) {
		Optional<Taco> taco = tacoRepo.findById(id);

		if (taco.isPresent()) {
			tacoRepo.deleteById(id);
			log.info("TACO DELETED");
			return ResponseEntity.noContent().build();
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}

}
