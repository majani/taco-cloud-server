package com.taco.rest.api.tacocloud.services;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.taco.rest.api.tacocloud.models.Taco;

public interface TacoService {

	Taco getTaco();

	List<Taco> getTacos();

	ResponseEntity<Void> createTaco();

	ResponseEntity<Taco> updateTaco();
	
	ResponseEntity<Void> deleteTaco();

	Iterable<Taco> recentTacos();

}
