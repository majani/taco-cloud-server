package com.taco.rest.api.tacocloud.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.taco.rest.api.tacocloud.models.Taco;

public interface TacoRepository extends JpaRepository<Taco, Long> {
}