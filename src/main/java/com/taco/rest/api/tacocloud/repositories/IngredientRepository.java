package com.taco.rest.api.tacocloud.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.taco.rest.api.tacocloud.models.Ingredient;

public interface IngredientRepository extends JpaRepository<Ingredient, String> {

}
